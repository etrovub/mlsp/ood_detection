import numpy as np
import scipy.stats as sp
import matplotlib.pyplot as plt
from sklearn.neighbors import KernelDensity


from pyod.utils.data import generate_data



def create_data():
    X_train, y_train, X_test, y_test = generate_data(n_features=100,
                      random_state=3)

    return X_train,y_train,X_test,y_test



def density_ratio_expts():
    m = [0.,1]
    s = [1.0,2.0]
    np.random.seed(10)
    x = np.linspace(-10,10,500)
    x1 = sp.norm.pdf(x,m[0], s[0])
    x2 = sp.norm.pdf(x, m[1],s[1])

    r=x1/x2

    plt.figure()
    plt.plot(x,r,label='true ratio')
    plt.show()


X_train,y_train,X_test,y_test=create_data()


